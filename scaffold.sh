#!/bin/bash

set -euo pipefail

source .env

if [[ -d "$PROJECT_NAME" ]]; then
  exit
fi

django-admin startproject $PROJECT_NAME
secret_key=$(python -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())')
sed -i "s/DJANGO_SECRET_KEY=*$/DJANGO_SECRET_KEY='$secret_key'/" .env
cd $PROJECT_NAME
python manage.py startapp $APP_NAME
mkdir -p $APP_NAME/management/commands/
cd ..
cp settings.py.template $PROJECT_NAME/$PROJECT_NAME/settings.py
python $PROJECT_NAME/manage.py migrate
python $PROJECT_NAME/manage.py createsuperuser --noinput
cp consumemessages.py $PROJECT_NAME/$APP_NAME/management/commands/
